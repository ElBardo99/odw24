## Environment

It's recommanded to run the tutorials in a virtual environment of a `conda` environment.
The `IGWN` conda environment is advised: [documentation](https://computing.docs.ligo.org/conda/).
Instructions on setting up your own environment are available [on this page](../../setup.md) (option 3).
However, it's ok to run all of the tutorials using pip. Currently Pyhough can only be installed from [PyPI(https://pypi.org/project/pyhough/).
